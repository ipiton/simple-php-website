FROM php:alpine3.12
EXPOSE 80
RUN mkdir /app
WORKDIR /app

COPY . /app/

CMD php -S 0.0.0.0:80
